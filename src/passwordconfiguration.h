/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Lukas Vogel <lukedirtwalker@gmail.com>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef PASSWORDCONFIGURATION_H
#define PASSWORDCONFIGURATION_H

#include <QString>

/*!
    \struct PasswordConfiguration
    \brief  Holds the Password configuration settings
 */
struct PasswordConfiguration {
    /**
     * \brief The selected character set.
     */
    enum characterSet {
        ALLCHARS = 0,
        ALPHABETICAL,
        ALPHANUMERIC,
        CHARSETS_COUNT //  have to be last, for easier initialization of arrays
    } selected;
    /**
     * \brief Length of the password.
     */
    int length;
    /**
     * \brief The different character sets.
     */
    QString Characters[CHARSETS_COUNT];
    PasswordConfiguration()
        : selected(ALLCHARS)
        , length(16)
    {
        Characters[ALLCHARS] = QStringLiteral(
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~!@#$%^&"
            "*()_-+={}[]|:;<>,.?"); /*AllChars*/
        Characters[ALPHABETICAL] = QStringLiteral(
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstu"
            "vwxyz"); /*Only Alphabetical*/
        Characters[ALPHANUMERIC] = QStringLiteral(
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstu"
            "vwxyz1234567890"); /*Alphabetical and Numerical*/
    }
};

#endif // PASSWORDCONFIGURATION_H
