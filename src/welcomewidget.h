// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QLabel>

namespace Ui
{
class WelcomeWidget;
}

class WelcomeWidget : public QWidget
{
    Q_OBJECT
public:
    WelcomeWidget(QWidget *parent = nullptr);
    ~WelcomeWidget();

private:
    QScopedPointer<Ui::WelcomeWidget> ui;
};
