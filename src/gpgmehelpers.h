/*
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef GPGMEHELPERS_H
#define GPGMEHELPERS_H
#include <QString>
#include <gpgme++/error.h>
#include <gpgme++/key.h>

inline bool isSuccess(const GpgME::Error &err)
{
    if (err) {
        return false;
    }
    if (err.isCanceled()) {
        return false;
    }
    return true;
}

inline QString fromGpgmeCharStar(const char *data)
{
    if (data) {
        return QString::fromLocal8Bit(data);
    }
    return QString{};
}

inline QString createCombinedNameString(const GpgME::UserID &id)
{
    auto name = fromGpgmeCharStar(id.name());
    auto comment = fromGpgmeCharStar(id.comment());
    auto email = fromGpgmeCharStar(id.email());
    if (comment.isEmpty()) {
        return QStringLiteral("%1 <%2>").arg(name, email);
    }
    return QStringLiteral("%1 (%2) <%3>").arg(name, comment, email);
}

#endif // GPGMEHELPERS_H
